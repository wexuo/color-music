# color-music

#### 介绍
基于MetingJS 的小程序音乐播放器，前端使用 ColorUI ，界面参考 QQ 音乐网页版移动端，后端基于 MetingJS 的接口，同时采用缓存减少对服务器的压力。此项目仅供学习使用，请勿用作商业用途。

#### 功能
- [x] 歌曲播放
- [x] 歌曲暂停
- [x] 上/下一曲
- [x] 歌词滚动
- [x] 进度条拖动
- [x] 正在播放
- [x] 播放历史
- [x] 歌曲排行榜
- [x] 歌曲搜索



#### 技术

- [x] [小程序背景音乐API  ](https://developers.weixin.qq.com/miniprogram/dev/api/media/background-audio/wx.getBackgroundAudioManager.html)
- [x] [ColorUI](https://github.com/weilanwl/ColorUI)
- [x] [MetingJS ](https://github.com/metowolf/MetingJS )
- [x] [MKOnlineMusicPlayer]( https://github.com/mengkunsoft/MKOnlineMusicPlayer)




#### 界面

![](https://cdn.jsdelivr.net/gh/laoxuai/images/image/20200531121130401.jpg)