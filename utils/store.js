/**
 * 缓存处理, 加入过期时间
 */
const cache = 'cache-'

/**
 * 设置键值存入缓存
 * @param {*} key 键
 * @param {*} data 值
 * @param {*} expiration 过期时间（秒）
 */
function set(key, data, expiration) {
  key = key + ''
  wx.setStorageSync(key, data)
  let seconds = parseInt(expiration)
  if (seconds > 0) {
    let time = Date.parse(new Date())
    let exp_time = time / 1000 + seconds
    wx.setStorageSync(cache + key, exp_time) // 当前 key 的过期时间
  } else {
    wx.removeStorageSync(cache + key)
  }
}

/**
 * 获取缓存中键的值
 * @param {*} key 键
 */
function get(key) {
  key = key + ''
  // 判断是否过期
  let exp_time = wx.getStorageSync(cache + key)
  let time = parseInt(exp_time)
  if (time && time < Date.parse(new Date()) / 1000) {
    remove(key)
    return null
  }
  let data = wx.getStorageSync(key)
  return data ? data : null
}

/**
 * 移除缓存中的键值对
 * @param {*} key 键
 */
function remove(key) {
  key = key + ''
  wx.removeStorageSync(key)
  wx.removeStorageSync(cache + key)
}

/**
 * 清空缓存
 */
function clear() {
  wx.clearStorageSync()
}

module.exports = {
  set,
  get,
  remove,
  clear
}