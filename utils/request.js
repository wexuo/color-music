/**
 * GET请求
 */
function get(url, data = {}) {
  return request(url, data, 'GET')
}

/**
 * POST请求
 */
function post(url, data = {}) {
  return request(url, data, 'POST')
}

function request(url, data = {}, method = "GET") {
  let contentType = 'application/json'
  return new Promise(function(resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': contentType
      },
      success: function(res) {
        resolve(res.data);
      },
      fail: function(error) {
        reject(error)
      }
    })
  });
}

module.exports = {
  get,
  post
}
